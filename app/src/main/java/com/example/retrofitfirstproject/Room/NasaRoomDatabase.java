package com.example.retrofitfirstproject.Room;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.retrofitfirstproject.Dao.NasaDao;
import com.example.retrofitfirstproject.Entity.Nasa;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

@Database(entities = {Nasa.class},version = 1)
public abstract class NasaRoomDatabase extends RoomDatabase {

    private static final String DATABASE_NAME="nasa_database";
    public abstract NasaDao nasaDao();
    public static volatile NasaRoomDatabase instance;
    public static Executor executor = Executors.newSingleThreadExecutor();

    public static NasaRoomDatabase getInstance(Context context) {


        if (instance == null) {
            synchronized (NasaRoomDatabase.class) {
                if (instance == null) {
                    instance = Room.databaseBuilder(context, NasaRoomDatabase.class,
                            DATABASE_NAME)
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return instance;

    }


}
