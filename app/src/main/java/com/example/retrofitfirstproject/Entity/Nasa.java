package com.example.retrofitfirstproject.Entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;


import com.google.gson.annotations.SerializedName;

@Entity(tableName = "nasa_table")
public class Nasa {

    @PrimaryKey(autoGenerate = true)
    private Integer id;


    private String explanation;


    private String title;


    private String url;


    private String date;


    private String media_type;




    
    public Nasa(String explanation, String url, String title, String date, String media_type){
        this.explanation=explanation;
        this.url=url;

        this.title=title;
        this.date=date;
        this.media_type=media_type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getExplanation() {
        return explanation;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMedia_type() {
        return media_type;
    }

    public void setMedia_type(String media_type) {
        this.media_type =media_type ;
    }



    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString(){
        return "DataModel{"+
                "tittle" + title +
                ",url"+url+
                ",explanation"+explanation+
                ",media_type"+media_type+
                ",date"+date+
                '}';

    }




}
