package com.example.retrofitfirstproject.Adapter;

import android.annotation.SuppressLint;
import android.app.AlertDialog;

import android.content.Context;

import android.content.Intent;


import android.graphics.Color;
import android.net.Uri;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import android.widget.ImageView;

import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.retrofitfirstproject.Entity.Nasa;
import com.example.retrofitfirstproject.R;
import com.example.retrofitfirstproject.databinding.ListLayoutBinding;

import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {

 List<Nasa> nasaList;
    private Context context;
    String type="image";
    private final OnClickListener onClickListener;
    private static final DiffUtil.ItemCallback<Nasa> List_OF_Nasa =new DiffUtil.ItemCallback<Nasa>() {
        @Override
        public boolean areItemsTheSame(@NonNull Nasa oldItem, @NonNull Nasa newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @SuppressLint("DiffUtilEquals")
        @Override
        public boolean areContentsTheSame(@NonNull Nasa oldItem, @NonNull Nasa newItem) {
            return oldItem.equals(newItem);
        }
    };
    private AsyncListDiffer<Nasa> differ = new AsyncListDiffer<Nasa>(this, List_OF_Nasa);

    public RecyclerAdapter(List<Nasa> nasaList ,OnClickListener onClickListener) {
        this.nasaList = nasaList;
        this.onClickListener = onClickListener;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //Inflate Layout
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ListLayoutBinding listLayoutBinding =ListLayoutBinding.inflate(layoutInflater,parent,false);
        final MyViewHolder myViewHolder = new MyViewHolder(listLayoutBinding);
        final Nasa nasa = nasaList.get(viewType);
        context = parent.getContext();

        myViewHolder.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onButtonClick(v);
                //ImageView images;
                AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                View view = LayoutInflater.from(context).inflate(R.layout.dialog_layout, null);
                TextView tvTitle = (TextView) view.findViewById(R.id.TV_TittleM);
                tvTitle.setText(nasa.getTitle());

                //image insert

                ImageView images = (ImageView) view.findViewById(R.id.IV_ImagesD);

                String media = nasa.getMedia_type();
                if(media.equals(type)) {
                    Glide.with(context).load(nasa.getUrl()).placeholder(R.drawable.loading).into(images);
                } else {
                    Glide.with(context).load(nasa.getUrl()).placeholder(R.drawable.download).into(images);
                }
                TextView tvMedia = (TextView) view.findViewById(R.id.TV_MediaType);
                tvMedia.setText(nasa.getMedia_type());
                TextView textPlan=(TextView)view.findViewById(R.id.TV_Explanation);
                String link = nasa.getUrl();
                SpannableString ss1 =new SpannableString(link);
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(@NonNull View widget) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                        browserIntent.setData (Uri.parse(nasa.getUrl()));
                        context.startActivity(browserIntent);
                    }

                    @Override
                    public void updateDrawState(@NonNull TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setColor(Color.BLUE);
                        ds.setUnderlineText(false);
                    }
                };
                ss1.setSpan(clickableSpan,0,link.length(),Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                textPlan.setText(nasa.getExplanation()+"\n");
                textPlan.append(ss1);
                textPlan.setMovementMethod(LinkMovementMethod.getInstance());
                textPlan.setHighlightColor(Color.BLUE);
                builder.setView(view);
                final AlertDialog alertDialog = builder.create();
                alertDialog.show();
                ImageView close = (ImageView) view.findViewById(R.id.IV_Close);
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });
                Window window = alertDialog.getWindow();
                window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
            }
        });


        return myViewHolder;

    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final Nasa nasa = nasaList.get(position);

        holder.title.setText(nasa.getTitle());
        holder.date.setText(nasa.getDate());

        //image insert recyclerView in thumbnail

        String media = nasa.getMedia_type();

        if(media.equals(type)) {
            //image thumbnail
            Glide.with(context).load(nasa.getUrl()).placeholder(R.drawable.loading).into(holder.image);
        }else {
            //video thumbnail
            Glide.with(context).load(nasa.getUrl()).placeholder(R.drawable.download).into(holder.image);
            holder.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                    browserIntent.setData (Uri.parse(nasa.getUrl()));
                    context.startActivity(browserIntent);
                }
            });

        }

    }

    @Override
    public int getItemCount() {
        return differ.getCurrentList().size();
    }

    public void getALLModelData(List<Nasa> nasaList)
    {
        this.nasaList = nasaList;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title,date;
        ImageView image;
        Button more;
        ListLayoutBinding listLayoutBinding;
        public MyViewHolder(@NonNull ListLayoutBinding listLayoutBinding) {
            super(listLayoutBinding.getRoot());
            this.listLayoutBinding=listLayoutBinding;

            title = listLayoutBinding.itemTitle;
            date = listLayoutBinding.itemDate;
            more = listLayoutBinding.btnMore;
            image = listLayoutBinding.IVMedia;
        }
    }
    public void updateList(List<Nasa>  newList) {
        differ.submitList(newList);
        // DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new MyDiffUtilCallback(this.webList, newList));
        //diffResult.dispatchUpdatesTo(this);
    }

    public interface OnClickListener{
        void onButtonClick(View v);

    }
}