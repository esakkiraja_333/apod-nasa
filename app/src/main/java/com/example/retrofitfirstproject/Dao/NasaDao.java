package com.example.retrofitfirstproject.Dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.retrofitfirstproject.Entity.Nasa;

import java.util.List;

@Dao
public interface NasaDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<Nasa> nasaList);

    @Query("SELECT * FROM nasa_table ORDER BY id DESC")
    LiveData<List<Nasa>> getAllModelData();

}

