package com.example.retrofitfirstproject.Ui;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.retrofitfirstproject.Entity.Nasa;
import com.example.retrofitfirstproject.Repositry.NasaRepository;

import java.util.List;

public class MainViewModel extends AndroidViewModel {
    private NasaRepository repository;

    private LiveData<List<Nasa>> nasaModel;

    public MainViewModel(@NonNull Application application) {
        super(application);

        repository = new NasaRepository(application);

    }
    public void NasaCall(String count){
    repository.nasaRequest(count);
    }

    public LiveData<List<Nasa>> nasaModel(){
        nasaModel = repository.getAllModelData();
        return nasaModel;
    }

}
