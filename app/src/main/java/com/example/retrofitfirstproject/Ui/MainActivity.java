package com.example.retrofitfirstproject.Ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.retrofitfirstproject.Adapter.RecyclerAdapter;
import com.example.retrofitfirstproject.Entity.Nasa;
import com.example.retrofitfirstproject.R;
import com.example.retrofitfirstproject.databinding.ActivityMainBinding;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textview.MaterialTextView;


import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,RecyclerAdapter.OnClickListener {

    MaterialTextView tittle;


    MaterialButton button1,button2,button3,button4;
    RecyclerView recyclerView;
    ActivityMainBinding activityMainBinding;
    RecyclerAdapter recyclerAdapter;
    MainViewModel mainViewModel;
    public String count = "";
    List<Nasa> nasList = new ArrayList<>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding = ActivityMainBinding.inflate(getLayoutInflater());

        View view = activityMainBinding.getRoot();
        setContentView(view);


        tittle = activityMainBinding.TVTittle;
        button1 = activityMainBinding.BT1;
        button2 = activityMainBinding.BT2;
        button3 = activityMainBinding.BT3;
        button4 = activityMainBinding.BT4;
        recyclerView = activityMainBinding.recyclerview;

        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
        button4.setOnClickListener(this);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerAdapter = new RecyclerAdapter(nasList,this);
//        activityMainBinding.recyclerview.setAdapter(recyclerAdapter);


        mainViewModel = new ViewModelProvider(this).get(MainViewModel.class);
        mainViewModel.nasaModel().observe(this, new Observer<List<Nasa>>() {
            @Override
            public void onChanged(List<Nasa> nasaList) {

                if (nasaList.isEmpty()) {
                    tittle.setVisibility(View.VISIBLE);
                } else {
                    recyclerView.setAdapter(recyclerAdapter);
                    recyclerAdapter.getALLModelData(nasaList);
                    recyclerAdapter.updateList(nasaList);
                    tittle.setVisibility(View.INVISIBLE);

                    Log.d("main", "onChanged: " + nasaList);
                }

            }


        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.BT_1:
                count = "1";
                break;

            case R.id.BT_2:

                count = "2";
                break;

            case R.id.BT_3:

                count = "3";
                break;

            case R.id.BT_4:

                count = "4";
                break;


        }
        MaterialButton button = (MaterialButton) v;
        String count =button.getText().toString();
        mainViewModel.NasaCall(count);

    }
    @Override
    public void onButtonClick(View v) {

    }
}
