package com.example.retrofitfirstproject.Service;

import com.example.retrofitfirstproject.Entity.Nasa;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RetrofitService {

 @GET("apod")
    Call<List<Nasa>> getAllModelData(@Query("api_key")String apiKey, @Query("count")String count);
}
