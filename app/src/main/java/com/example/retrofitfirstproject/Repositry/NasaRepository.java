package com.example.retrofitfirstproject.Repositry;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.retrofitfirstproject.Entity.Nasa;
import com.example.retrofitfirstproject.Retrofit.RetrofitClient;
import com.example.retrofitfirstproject.Room.NasaRoomDatabase;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NasaRepository {
    public String apikey ="goLj8jNbuSbNDt3IRwbLGyModq3yUWPKob5zzao7";
    private LiveData<List<Nasa>> getAllModelData;
    private NasaRoomDatabase nasaRoomDatabase;
    //public Context context;
    //private MutableLiveData<String> toastMessageObserver = new MutableLiveData();
    public NasaRepository(Application application){
        nasaRoomDatabase = NasaRoomDatabase.getInstance(application);


    }
    public  void NasaInsert(List<Nasa> nasaList){
        getLists(nasaList);
    }

    public LiveData<List<Nasa>> getAllModelData(){
        getAllModelData = nasaRoomDatabase.nasaDao().getAllModelData();
        return getAllModelData;
    }

    public void getLists(final List<Nasa> lists){
        nasaRoomDatabase.executor.execute(new Runnable() {
            @Override
            public void run() {
            nasaRoomDatabase.nasaDao().insert(lists);
            }
        });
    }


    public void nasaRequest(String count){
        RetrofitClient retrofitClient = RetrofitClient.getInstance();
        Call<List<Nasa>> call =retrofitClient.apiInterface().getAllModelData(apikey,count);
        call.enqueue(new Callback<List<Nasa>>() {

            @Override
            public void onResponse(Call<List<Nasa>> call, Response<List<Nasa>> response) {

                if(response.isSuccessful()){

                    NasaInsert(response.body());
                    Log.d("main","onResponse"+response.body());
                    //toastMessageObserver.setValue("Data inserted successfully"+response.message());

                }
                else {
                    response.code();
                }
            }

            @Override
            public void onFailure(Call<List<Nasa>> call, Throwable t) {
                Log.d("main", "onFailure: ");

            }
        });
    }


}
